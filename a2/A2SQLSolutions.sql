select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person as p, employee as e, faculty as f 
where p.per_id=e.per_id
and e.per_id=f.per_id;


select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person p
  join employee e on p.per_id=e.per_id;
  join faculty f on e.per_id=f.per_id;

select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person
  join employee using (per_id)
  join faculty using (per_id);

select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person
  natural join employee
  natural join faculty;