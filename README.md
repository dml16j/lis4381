# LIS4381 - Mobile Web Application Development

## Duncan Lopatine

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Ampps
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android App
    - Provide Screenshots of completed application

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links
    - Include Mobile Web App Concert Screenshots
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Provide Screenshots
    - Provide skillsets 10, 11, and 12
    - Create an interactive webpage that allows you to enter in data for Petstore Form
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Provide Screenshots
    - Provide skillsets 13, 14, and 15
    - Displays user-entered data
    - Allow users to add data
    
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Read and Answer Questions from Chapter 7 & 8
    - Code and create an interactive application that provides contact information
    - Provide screenshots
    - Provide Skillsets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create an RSS Feed
    - Make a Delete and Edit Button on the P2 project
    - Edit descriptions of the website
    - Provide Screenshots

