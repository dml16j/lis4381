# LIS4381 - Mobile Web Application Development
## Duncan Lopatine

### Assignment 5 Requirements:

*Four Parts:*

1. Create Website
2. Modify Code for A5
3. Put two photos on home page
4. Make Skillsets 

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshots of website
* Link to LIS4381 website: http://localhost/repos/lis4381/

### Assignment Screenshots:
|   Screenshot of Error Page:  |
|------------------------------------------------|
|![ErrorPage](img/img.jpg)             |

|   Screenshot of active A5 Data:  |
|----------------------------------------|
|![Screen 1](img/img1.jpg)   |


|  Screenshot of SS13:   |
|-----------------------------------------|
|  ![Screen 2](img/ss13.jpg) |


|   Screenshot of Skillset 14 Part 1:  |
|-------------------------------------------|
|![Animals](img/ss14_1.jpg)          |

|  Screenshot of Skillset 14 Part 2:   |
|----------------------------------------------|
|  ![ArrayIntegers](img/ss14_2.jpg)    |



|   Screenshot of Skillset 14 Part 3:  |
|------------------------------------------------|
|![Part3](img/ss14_3.jpg)             |



|   Screenshot of Skillset 14 Part 4:  |
|------------------------------------------------|
|![Part4](img/ss14_4.jpg)             |


|   Screenshot of Skillset 15 Part 1:  |
|------------------------------------------------|
|![Part1](img/ss15_1.jpg)             |


|   Screenshot of Skillset 15 Part 2:  |
|------------------------------------------------|
|![Part2](img/ss15_2.jpg)             |

