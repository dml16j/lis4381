# LIS4381 - Mobile Web Application Development
## Duncan Lopatine

### Project 1 Requirements:

*Three Parts:*

1. Watch video and create Android Studio application
2. Provide screenshots of the applications
3. Chapter Questions (Ch's 7, 8)

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Created an interactive Mobile Web App that included a photo & contact information
* Screenshot of running application's first and second user interface
* Include Mobile Web App Contact Screenshots

### Assignment Screenshots:
|   Screenshot of first user interface:  |  Screenshot of second user interface:   |
|----------------------------------------|-----------------------------------------|
|![Screen 1](img/page1.jpg)   |  ![Screen 2](img/page2.jpg) |


|   Screenshot of Skillset 7:  |  Screenshot of Skillset 8:   |
|-------------------------------------------|----------------------------------------------|
|![Min & Max Integers](img/skillset7.jpg)          |  ![Largest of 3 Integers](img/skillset8.jpg)    |


|   Screenshot of Skillset 9:  |
|------------------------------------------------|
|![Array Size](img/skillset9.jpg)             |
