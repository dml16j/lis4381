# LIS4381 - Mobile Web Application Development
## Duncan Lopatine

### Assignment 3 Requirements:

*Three Parts:*

1. Watch video and create Android Studio application
2. Provide screenshots of the applications
3. Chapter Questions (Ch's 5, 6)

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshot of running application's first user interface
* Create ERD based upon business rules
* Provide screenshot of completed ERD
* Provide DB resource links
* Include Mobile Web App Concert Screenshots

### Assignment Screenshots:
|   Screenshot of ERD Diagram:  |
|------------------------------------------------|
|![ERD](img/sql.jpg)             |

|   Screenshot of first user interface:  |  Screenshot of second user interface:   |
|----------------------------------------|-----------------------------------------|
|![Screen 1](img/screen1.jpg)   |  ![Screen 2](img/screen2.jpg) |


|   Screenshot of Skillset 4:  |  Screenshot of Skillset 5:   |
|-------------------------------------------|----------------------------------------------|
|![Even or Odd](img/ss4.jpg)          |  ![Largest Number](img/ss5.jpg)    |


|   Screenshot of Skillset 6:  |
|------------------------------------------------|
|![Arrays and Loops](img/ss6.jpg)             |
