# LIS4381 - Mobile Web Application Development
## Duncan Lopatine

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch's 1, 2)

#### README.md file should include the following items:

* Screenshot of Ampps Installation My PHP Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio: My First App
* Git commands with short descriptions
* Bitbucket repo links. a) this assignment and b) the completed tutorial(bitbucketstationlocations)

### Git commands w/short descriptions:

1. Git Init - Used to start a new repository.
2. Git Status - Lists all the files that have to be committed.
3. Git Add - Adds a file to the staging area.
4. Git Commit - Records or snapshots the file permanently in the version history. 
5. Git Push - Sends the branch commits to your remote repository.
6. Git Pull - Fetches and merges changes on the remote server to your working directory.
7. Git Clone - Used to obtain a repository from an existing URL.


### Assignment Screenshots:

*Screenshot of AMPPS Installation* [*My PHP Installation*](http://localhost:8080 "PHP Localhost")

![AMPPS Installation Screenshot](img/ampps.jpg)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/a1jdk.jpg)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.jpg)


### Tutorial Link:

*Bitbucket Tutorial:*
[A1 BitbucketStationLocations Tutorial](https://bitbucket.org/dml16j/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
