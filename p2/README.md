# LIS4381 - Mobile Web Application Development
## Duncan Lopatine

### Project 2 Requirements:

*Four Parts:*

1. Create Website
2. Modify Code for P2
3. Put five photos on LocalHost page 

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshots of website
* Link to LIS4381 website: http://localhost/repos/lis4381/

### Assignment Screenshots:
|   Screenshot of Index Page:  |
|------------------------------------------------|
|![IndexPage](img/ss3.jpg)             |

|   Screenshot of Edit Page:  |
|----------------------------------------|
|![EditPG](img/ss2.jpg)   |


|  Screenshot of Error Page:   |
|-----------------------------------------|
|  ![Errorpage](img/ss_error.jpg) |


|   Screenshot of Home Page:  |
|-------------------------------------------|
|![Homepage](img/ss_home.jpg)          |

|  Screenshot of RSS Feed:   |
|----------------------------------------------|
|  ![RSS_feed](img/ss1.jpg)    |
