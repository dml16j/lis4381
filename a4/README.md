# LIS4381 - Mobile Web Application Development
## Duncan Lopatine

### Assignment 4 Requirements:

*Four Parts:*

1. Create Website
2. Modify Code for A4
3. Put three photos on home page
4. Make Skillsets 10-12

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshots of website
* Link to LIS4381 website: http://localhost/repos/lis4381/

### Assignment Screenshots:
|   Screenshot of Main Page:  |
|------------------------------------------------|
|![MainPage](img/img1.jpg)             |

|   Screenshot of Inactive A4 Data:  |  Screenshot of Active A4 Data:   |
|----------------------------------------|-----------------------------------------|
|![Screen 1](img/img2.jpg)   |  ![Screen 2](img/img3.jpg) |


|   Screenshot of Skillset 10:  |  Screenshot of Skillset 11:   |
|-------------------------------------------|----------------------------------------------|
|![Animals](img/img4.jpg)          |  ![ArrayIntegers](img/img5.jpg)    |


|   Screenshot of Skillset 12:  |
|------------------------------------------------|
|![Temperature](img/img6.jpg)             |

