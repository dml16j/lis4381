import java.util.Scanner;

public class Methods {
    //create Method without returning any value (without object)
    public static void getRequirements() {
        //display operational messages
        System.out.print("Developer: Duncan Lopatine ");
        System.out.println();
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements.");
        System.out.println("2) getUserinput(): Void method prompts for user input, \n\tthen calls two methods: myVoidMethod() and myValueReturningMethod().");
        System.out.println("3) myVoidMethod():\n" + "\ta. Accepts two arguments: String and int. \n" + "\tb. Prints user's first name and age.");
        System.out.println("4) myValueReturningMethod():\n" + "\ta. Accepts two arguments: String and int. \n" + "\tb. Returns String containing first name and age.");
        System.out.println(); //newline
    }

    public static void getUserInput() {
        //initialize variables, create Scanner object, capture user input
        String firstName = "";
        int userAge = 0;
        String myStr = "";
        Scanner sc = new Scanner(System.in);

        //input
        System.out.print("Enter first name: ");
        firstName = sc.next();

        System.out.print("Enter age: ");
        userAge = sc.nextInt();

        System.out.println(); //newline

        //call void method
        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        //call value-returning method
        System.out.print("value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }

    public static void myVoidMethod(String first, int age) {
        System.out.println(first + " is " + age);
    }

    public static String myValueReturningMethod(String first, int age) {
        return first + " is " + age;
    }
}